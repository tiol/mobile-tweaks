# Work around rendering artifacts on bitfrost until mesa is fixed (#11892)

GPUPATH=/sys/class/drm/card?/device/of_node/compatible

if grep -q --binary-files=text "arm,mali-bifrost" $GPUPATH; then
    export GDK_GL_DISABLE=base-instance
fi
