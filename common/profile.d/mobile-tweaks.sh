# Help some windowing systems along until they use wayland by default
if [ "$XDG_SESSION_TYPE" = "wayland" ]; then
    export QT_QPA_PLATFORM
    QT_QPA_PLATFORM=wayland

    export QT_WAYLAND_DISABLE_WINDOWDECORATION
    QT_WAYLAND_DISABLE_WINDOWDECORATION=1

    export MOZ_ENABLE_WAYLAND
    MOZ_ENABLE_WAYLAND=1
fi

# Set environment variables needed by Kirigami apps in non-Plasma environments
if [ "$XDG_SESSION_DESKTOP" != "plasma-mobile" ] && [ "$XDG_CURRENT_DESKTOP" != "KDE" ]; then
    export QT_QUICK_CONTROLS_STYLE
    QT_QUICK_CONTROLS_STYLE=org.kde.breeze

    export QT_QUICK_CONTROLS_MOBILE
    QT_QUICK_CONTROLS_MOBILE=1
fi
